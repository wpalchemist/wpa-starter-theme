<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">  -->
    <title><?php wp_title(); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" media="screen, projection" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
     
    <script src="<?php bloginfo('template_directory'); ?>/js/modernizr-1.6.min.js"></script>
     
    <?php wp_head(); ?>
</head>
 
<body <?php body_class(); ?>>
 
    <header id="top">
        <h1><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
        <p class="description"><?php bloginfo( 'description' ); ?></p>
        <a class="offscreen" href="#content">Skip to main content</a>
        <?php wp_nav_menu( array( 'container' => 'nav', 'fallback_cb' => 'wpa_menu', 'theme_location' => 'primary' ) ); ?>
    </header>

    <div id="content">