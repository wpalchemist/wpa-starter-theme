	</div><!-- #content -->
	<footer id="bottom">

		<a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
			<?php bloginfo( 'name' ); ?>
		</a>

		<?php do_action( 'wpa_credits' ); ?>
		
		<a href="<?php echo esc_url( __('http://wordpress.org/', 'wpa') ); ?>" title="<?php esc_attr_e('Semantic Personal Publishing Platform', 'wpa'); ?>" rel="generator"> 
			<?php printf( __('Proudly powered by %s.', 'wpa'), 'WordPress' ); ?>
		</a>

	</footer>

<?php wp_footer(); ?>
</body>
</html>