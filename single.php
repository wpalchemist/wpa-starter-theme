<?php get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<nav>
			<span class="older"><?php previous_post_link( '%link', '' . _x( '&larr;', 'Previous post link', 'wpa' ) . ' %title' ); ?></span>
			<span class="newer"><?php next_post_link( '%link', '%title ' . _x( '&rarr;', 'Next post link', 'wpa' ) . '' ); ?></span>
		</nav>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<header>
				<h1><?php the_title(); ?></h1>

				<?php wpa_posted_on(); ?>
			</header>

			<?php the_content(); ?>
					
			<?php wp_link_pages( array( 'before' => '<nav>' . __( 'Pages:', 'wpa' ), 'after' => '</nav>' ) ); ?>
		
			<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'wpa_author_bio_avatar_size', 60 ) ); ?>
				<h2><?php printf( esc_attr__( 'About %s', 'wpa' ), get_the_author() ); ?></h2>
				<?php the_author_meta( 'description' ); ?>
					<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
						<?php printf( __( 'View all posts by %s &rarr;', 'wpa' ), get_the_author() ); ?>
					</a>
			<?php endif; ?>
			
			<footer>
				<?php wpa_posted_in(); ?>
				<?php edit_post_link( __( 'Edit', 'wpa' ), '', '' ); ?>
			</footer>
				
		</article>

		<nav>
			<span class="older"><?php previous_post_link( '%link', '' . _x( '&larr;', 'Previous post link', 'wpa' ) . ' %title' ); ?></span>
			<span class="newer"><?php next_post_link( '%link', '%title ' . _x( '&rarr;', 'Next post link', 'wpa' ) . '' ); ?></span>
		</nav>

		<?php comments_template( '', true ); ?>

	<?php endwhile; // end of the loop. ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>